console.log("Hello World");

/*  [SECTION] Functions
    -   Functions in JavaScript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked.
    -   They are also used to prevent repeating lines/blocks of codes that perform the same task/function.
*/

/*  1. Function Declaractions
    -   function statement defines a function with the specified parameters.
     
    syntax:

        function functionName() {
            code block (statement)
        }

        "function" keyword - use to define a javascript function.

        functionName - the function name is used so we are able to call/invoke are declared function.

        function code block ({}) - statements which comprise the body of the function. This is where the code to be executed.
*/

function printName() {
    console.log("My name is John");
}

/*  [SECTION] Function Invocation
    
    -   It is common to use the term "call a function" instead of "invoke a function"
    
    -   Let's invoke/call the function that we have not was declared.    
*/

printName();

declaredFunction();  // - results in an error, much like variables, we cannot invoke a function that we have not define yet.

// declared functions can be hoisted. As long as the function has been definend
// Hoisting is JavaScript's behavior for certain variables(var) and functions to run or use them before their declare function.

function declaredFunction() {
    console.log("Hello World");
}



/*  [SECTION] Function Declaration vs Expression
        Function Declaraction
        - A function can be created through function declaraction by using the function keyword and adding a function name.


    2. Function Expression
    -   A function can be also stored in a variable. This is called as function expression

    -   A function expression is an anonymous function assigned to the variableFunction
        * Annonymous function - a function without a name.

    */

    // variableFunction(); // error - function expressions, beign stored in a let or const variable. and CANNOT BE USE GLOBAL

    let variableFunction = function() {
        console.log("Hello Again!");
    }

    variableFunction();



    // We can also create a function expression of a named function.
    let funcExpression = function funcName(){
        console.log("Hello from the other side.");
    }
    // funcExpression = declaredFunction();

    funcExpression();
    // funcName(); - result to an not defined function.

    declaredFunction();

    // Reassigning a declare function and expression.
    declaredFunction = function() {
        console.log("updated declared Function");
    }

    declaredFunction();
    

    funcExpression = function (){
        console.log("Updated funcExpression.");
    }

    funcExpression();
    
    
    const constantFunc = function(){
        console.log("Initialized with const");
    }

    constantFunc();

    /*  However, we cannot re-assign a function expression initialized with const.
    constantFunc = function(){
        console.log("New value");
    }

    constantFunc();
     */



/*  [SECTION] Function Scooping

    Scope - is the accessibility/visibility of a variable in the code.

    JavaScript Variables has 3 types of scope
    1. local/block scope
    2. global scope
    3. function scope
    
*/

    // Variables declared inside a {} block only can only access locally.
    // Local and Block scope only works with let and const.
    {
        let localVar = "Armdando Perez";
        //var localVar = "Armdando Perez";
        console.log(localVar);
    }


    let globalVar = "Mr. Worldwide";

    console.log(globalVar);
    // console.log(localVar);



    /* Function Scopes 
        Javascript has function scopes: Each function creates a new scope. 
        
    */

        // Variable defined inside a function are not accessible/visible outsite the function .
        // Variables declared with var, let and const are quite similar when declared inside a function. 
        function showNames (){
            //function scoped variables:
            var functionVar = "Joe";
            const functionConst = "John";
            let functionLet = "Jane";

            console.log(functionVar);
            console.log(functionConst);
            console.log(functionLet);
        }

        showNames();
/*      error - This are function scoped variable and cannot be accessed outside the function they were declared in.
        console.log(functionVar);
        console.log(functionConst);
        console.log(functionLet);
 */

        
/*  Nested Functions
    -   You can create another function inside a function.
    -   This is called a nested function.
*/

        function myNewFunction(){
            let name = "Jane";
            
            function nestedFunction(){
                let nestedName = "John";
                console.log(name);
            }
            // console.log(nestedName); // result to not defined error.
            nestedFunction();
        }

        myNewFunction();
        // nestedFunction(); // result to not defined error.

        // Function and Global Scope variables 

        // Global Scoped Variable
        let globalName = "Alexandro";
        function myNewFunction2(){
            let nameInside = "Renz";
            console.log(globalName);
        }

        myNewFunction2();
        // console.log(nameInside); Error - not defined



/* 
        [SECTION] Using alert();
        - alect() allows us to show a small window at the top of our browser page to show inforamation to our users.
        - It allows us to show a short dialog or instructions to our users. The page will wait until the user dismisses the dialog.

*/

        //alert("Hello World!"); // This will run immediately when the page loads.

        function showSampleAlert (){
            alert("Hello, User!");
        }

        //showSampleAlert();
        //console.log("I will only log in the console when the alert is dismissed.");

        // Notes on the use of alert():
            // Show only an alert() for short dialogs/messages to the user.
            // Do not overuse alert() because the program/JS has to wait for it to be dismissed before continuing.

/* 
        [SECTION] Using prompt()
        - prompt() allows us to show small window at the top of the browser to gather user input.
        - The input form the prompt() will be returned as a "String" once the user dismisses the window.

        Syntax:

            let variableName = prompt("<dialogString>")
*/

        // let samplePrompt = prompt("Enter your Full Name:")
        // console.log(typeof samplePrompt); To check the dataype of the prompt

        // console.log("Hello, " +samplePrompt);

        //let sampleNullPrompt = prompt ("Don't enter anything.");
        // prompt() returns an empty string("") when their is no input, or null if the user cancels the prompt.
        //console.log(sampleNullPrompt);

        // Let's create a function scope variable that will store the return data from our prompt().

        function printWelcomeMessage(){
            let firstName = prompt("Enter your first name: ");
            let lastName = prompt("Enter your last name: ");
            console.log("Hello, " + firstName + " " + lastName+ "!");
            console.log("Welcome to my page!");
        }

        printWelcomeMessage();




/* 
        [SECTION] Function Naming Convetion

        Function name should be definitive of the task it will perform. It usually contains a verb.
*/

        function getCourses() {
            let courses = ["Science 101", "Math 101", "English 101"]
            console.log(courses);
        }

        getCourses();

        // Avoid generic names to avoid confusion within our code.

        function get(){
            let name = "Jamie";
            console.log(name);
        }

        get();

        // Avoid pointless and inappropriate function names, example: foo, bar
        // This are "metasyntatic variable" which are set of words identified as a placeholder in computer programming.

        function foo() {
            console.log(25%5);
        }
        foo();

        // Name your functions in small caps. Follow camelCase when naming variables and functions.

        function displayCarInfo () {
            console.log("Brand: Toyota");
            console.log("Type: Sendan");
            console.log("Price: 1,500,000");
        }

        displayCarInfo();